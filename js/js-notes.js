// External JavaScript

// single line comment

/*
this
is
a
multi
line
comment
 */

// console.log(5+100)

// DATA TYPES
//Primitive Types
//numbers, strings, boolean, undefined, null...objects

// VARIABLES
// var, let, const

// DECLARING VARIABLES
// syntax: var nameOfTheVariable;
// syntax: var nameOfVariable = value;
// let nameOfVariable = value;
// const nameOfVariable = value;

// Declare a variable named firstname
var firstName;
// Assigned the value of "Billy" to firstname
firstname = "Billy";
// Called the variable name
console.log( firstname);
// alert(firstname)

// var firstname = "Billy";

// OPERATORS
/*

+ addition
- subtraction
* multiplication
/ divide
% modulus aka remainder

 */


console.log(5 % 4);
console.log((1 + 2) * 4 / 5)
// PEMDAS

// LOGICAL OPERATORS
// && AND
// || OR
// !  NOT
// return a boolean value when used with boolean values
// also used with non-boolean values

// AND &&
// TRUE STATEMENT && TRUE STATEMENT = (true)
// TRUE && FALSE = (false)
// False && False = (true)
// False && TRUE = (false)

// OR ||
// TRUE || TRUE = (true)
// TRUE || FALSE = (true)
// FALSE || FALSE = (false)
// FALSE || TRUE = (true)

// ! NOT ( the opposite)
// !TRUE = (false)
// !FALSE = (true)

// !!!!!!!TRUE = (false)


// =, ==, ===

// = used to assign value to a variable
var lastname = "Smith";

// COMPARISON OPERATOR
// == checks if the value is the same
// === checks if the value AND the data type are the same
var firstPet = "Dog";
var secondPet = "Cat";

console.log(firstPet == secondPet); // false

// var age = 12;
// var age2 = "12";
// console.log(age == age2); // true
// console.log(age === age2); // false

// != checks if the value is NOT the same
// !== checks if the value AND datatype is NOT the same
// console.log(age != age2); // false because age = 12 AND age2 = 12
// console.log(age !== age2); // true because age is a number while age2 is a string

// <, >, <=, >=
const age = 21;

console.log(age >= 21 || age < 18); // true
// TRUE || FALSE
console.log(age <= 21 || age > 18); // true
// TRUE || TRUE
console.log(age < 21 && age >18); // false
// FALSE && TRUE

// CONCATENATION
const person = "Bruce";
const person_age = 40;

console.log("Hello my name is " + person + " and I am " + person_age + " years old.");

// TEMPLATE STRINGS
console.log(`Hello my name is ${person} and I am ${person_age} years old.`)

// GET THE LENGTH OF A STRING
console.log(person.length); // 5

const greeting = "Hello World!";
console.log(greeting.length); // 12

// GET THE STRING IN UPPERCASE / LOWERCASE
console.log(greeting.toUpperCase()); // HELLO WORLD!

console.log(greeting.toLowerCase()); // hello world!

// SUBSTRING
console.log(greeting.substring(0, 5)); // Hello
console.log(greeting.substring(6, 11)); // World (no !)

// ello
console.log(greeting.substring(1, 5)); // ello


// FUNCTIONS
// - a reusable block of code the performs
// a specific task(s).

// syntax for function:
/*
 function nameOfTheFunction() {
    code you want the function to do
 }
* */

function sayHello() {
    console.log("Hello!!!");
}

// CALLING A FUNCTION
// when we want it to run, we call it by
// its name with the ()

sayHello();
// sayHello;

function addNumber(num1) {
    console.log(num1 + 2);
}

addNumber(21);


function message(name, age) {
    console.log(`Hello ${name} you are ${age} years old`)
}

message("Clark", 50)