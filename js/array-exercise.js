"use script"

// TODO: Create a new EMPTY array named sports. Push into this array 5 sports. Sort the array.
//  Reverse the sort that you did on the array. Push 3 more sports into the array.Locate your
//  favorite sport in the array and Log the index. Remove the 4th element in the array.
//  Remove the first sport in the array. Turn your array into a string.
// var sports = ['soccer', 'american football', 'tennis', 'track and field', 'skiing'];
//     console.log(sports.sort());
//     console.log(sports.reverse());
//         sports.push('polo', 'hockey', 'gaming');
//     console.log(sports);
//     console.log(sports[0]);
//     console.log(sports);
//         var removed = (sports.splice(3,1));
//             console.log(sports);
//     console.log(sports.shift());
//     console.log(sports.join(','));

// TODO: Create an array to hold your top singers/bands/etc.
//  const bands = ['AC/DC', 'Pink Floyd', 'Van Halen', 'Metallica'];
//  1. console log 'Van Halen'
//  2. add 'Fleetwood Mac' to the beginning of the array
//  3. add 'Journey' to the end of the array
// const band = ['AC/DC', "Pink Floyd", "Van Halen", "Metallica"];
//     console.log(band[2])
//         band.unshift('Fleetwood Mac');
//     console.log(band);
//         band.push('Journey');
//     console.log(band);

// TODO: Create a new array named cohort_name that holds the names of the people in your class
// var cohort_name = ['nick', 'josh', 'jose', 'stephen', 'karen'];
//     console.log(cohort_name);
//         console.log(cohort_name[1]);
//         console.log(cohort_name[2]);
//         console.log(cohort_name[3]);
//         console.log(cohort_name[4]);
//     for (var x = 0; x < cohort_name.length; x++) {
//         console.log(cohort_name[x]);
//     }
//     cohort_name.forEach(function (x) {
//         console.log(x);
//     });

// TODO: Using a for loop. Remove all instance of 'CodeBound' in the array
// var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"]
/*
index of 0 = "fizz"
index of 1 = "Codebound"
index of 2 = "buzz"

What is the VALUE of index 5?
    second "buzz"

What is the second ELEMENT?
    first "Codebound"
 */

// My solution
//     console.log(arrays1.length);
// for (var i = 0; i <= arrays1.length; i ++) {
// // removes index with the value of 'Codebound'
//     if (arrays1[i] !== "CodeBound") {
// // print out the elements of array1
//         console.log(arrays1[i]);
//     }
// }

// Another way to solve this problem with else
//     if (arrays1[i] === "CodeBound") {
//         console.log(" ");
//     }
//     else {
//         console.log(arrays1[i]);
//     }

// TODO: Using a forEach loop. Convert them to numbers then sum up all the numbers in the array
// var sum = 0;
// var numbers = [3, '12', 55, 9, '0', 99]
//     numbers.forEach(function (item){
//         sum += parseInt(item);
//     })
// console.log(sum)

// TODO: Using a forEach loop Log each element in the array while changing the value to 5 times the original value
// var numbers = [7, 10, 22, 87, 30]
//     numbers.forEach(function (item) {
//         console.log(item * 5);
//     })

// TODO: Create a function named removeFirstLast where you remove the first and last character of a string
// function removeFirstLast(x) {
//     var removeArray = x.split([""]);
//     removeArray.shift();
//     removeArray.pop();
//     return removeArray.join("");
// }
// console.log(removeFirstLast("Burger"))


// BONUS
// TODO: Write a function named phoneNum that accepts an array of 10 integers (between 0 and 9).
//  This will return a string of those numbers in a
//  *  phone number format.
//  *  phoneNum([7,0,8,9,3,2,0,1,2,3])=> returns "(708) 932-0123


// TODO: Create a function that will take in an integer as an argument and Returns
//  "Even" for even numbers and returns "Odd" for odd numbers


// TODO: Create a function named reverseString that will take in a string and reverse it.
//  There are many ways to do this using JavaScript built-in methods. For this exercise use a loop


// TODO: Create a function named converNum that will convert a group of numbers to a reversed array of numbers**
//  * 386543 => [3,4,5,6,8,3]
