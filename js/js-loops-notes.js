"use script"
// JAVASCRIPT LOOPS
// allows us to execute code repeatedly

// while loop, do-while loop, for loop, and
// break and continue

// while loop
// is a basic looping construct that will execute
// a block of code, as long as a certain
// condition is true

/*
syntax:
while (condition) {
    // run the code
}
*/

// example
// var num = 0;
//     while (num <= 100) {
//         console.log(`#` + num);
//     //    increment a value by 1...
//     //     num += 1;
//         num ++;
//     }


// do while loop
/*
the only difference from a while loop
is that the condition is evaluated
at the end of the loop.
Instead of the beginning
*/

/*syntax
do {
/// run the code
} while (condition)
*/

// example
// var i = 10;
// do {
//     console.log("Do-while #" + i);
//     // decrement by 1
//     // i -= 1;
//     i --;
// } while (i >= 0);

// for loop
/*
is a robust looping mechanism
available in many programming
 */

// syntax
/*
for (initialization; condition; increment/decrement) {
    // run the code
}
*/

// example
// for (var x = 100; x <= 200; x += 10) {
//     console.log("for loop #" + x);
// }

// BREAK AND CONTINUE
/*
- breaking out of loop
- using the 'break' keyword allows us to exit the loop
*/

// var endAtNumber = 5;
//
// for (var i = 1; i <= 100; i++) {
//
//     console.log("loop count #" + i);
//
//     // if statement
//     if (i === endAtNumber) {
//         alert("We have reached out limit. Break!");
//         break;
//         console.log("Do you see this message???");
//     //    ^ you will never see this console.log message...
//     }


// continue keyword

//  - continues the next iteration
// of a loop
for (var  n = 1; n <= 100; n++) {

    // if statement
    if (n % 2 !== 0) {
        continue;
    }

    console.log("Even Number; " + n);
    // prints out n % 2 === 0  (n % 2 !== 0) is meant to retrieve even numbers

}
