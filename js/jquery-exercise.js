"use strict";
// - Create content in your HTML
// - h1: your name
// - p: your hometown
// - an unordered list of your hobbies(4)


// - Add attributes to your elements, you will need both id and class attributes


// - Use jQuery to select an element by the id. Create an alert with the contents of the element
// var name = $('#name').html()
//     alert(name);

// - Update the jQuery code to select and alert a different id
// $(document).ready(function() {
//     $('.dropdown-button').on('click', function() {
//         var toggle = $(this);
//         $('#'+toggle.data('toggle')).toggle();
//     })
// });

// - Use the same id on 2 elements. How does this change the jQuery selection?
// The order in which an id is called will be first id with name the second id with duplicate name on first will not be called


// - Remove the duplicate id. Each id should be unique on that page.


// Class Selectors
// - Remove/comment out your custom jQuery code from the previous exercises.
// - Update your code so that at least 3 different elements have the same class name 'codebound'
// - Using jQuery, create a border around all elements with the class 'codebound' that is 2 pixels around and red
// $('.codebound').css('border', 'solid 2px red');

// - Remove the class from one of the elements. Refresh and test the border has been removed.
// Yes, the border has been removed.

// - Give another element an id of 'codebound'. Did this element get a border?
// No, the element did not receive the border.



//     Element Selectors
// - Remove/comment out your jQuery code from the previous lesson
// - Using jQuery, set the font-size of all 'li' elements to 24px
// $('li').css('font-size', '24px');

// - Change the font colors of all h1, p, and li element (all should be different colors)
// $('h1').css('color', 'blue');
// $('p').css('color', 'skyblue');
// $('li').css('color', 'orange');

// - Create a jQuery statement that alerts the contents of your h1 element(s)
// $('#name').html(alert(prompt('Enter your name ' + name)));
// var name = $('h1').html()
//     alert(name);

// Multiple Selectors
// - Change to font colors and sizes of all h1, p, and li elements to be the same.
// $('h1, p, li').css({'color': 'skyblue', 'font-size': '15px'});