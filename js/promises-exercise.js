(function () {
    "use strict";

// CREATE A FILE NAMED 'promises-exercise.js' INSIDE OF YOUR JS DIRECTORY
// Write a function name 'wait' that accepts a number as a parameter, and returns
// a PROMISE that RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
    // EX. wait(4000).then(() => console.log('You will see this after 4 seconds.'));
    // EX. wait(8000).then(() => console.log('You will see this after 8 seconds.'));

    // const wait = (time) => {
    //     return new Promise(((a, b) => {
    //         setTimeout(() => {
    //             a(`You will see this after ${time/1000} seconds.`)
    //         }, time);
    //     }))
    // }

// wait(6000).then(data => console.log('', data));

//Exercise 2:
//Write a function testNum that takes a number as an argument and returns a Promise
// that tests if the value is less than or greater than the value 10.

//     const i = 5
//     const testNum = new Promise((c, d) => {
//         if (i < 10) {
//             c('Value is less than 10');
//         }
//         else {
//             d('Value is greater than 10');
//         }
//     });
//     testNum.then(data => console.log('', data));
//     testNum.catch(error => console.log('', error));

//Exercise 3:
    //Write two functions that use Promises that you can chain! The first function,
    // makeAllCaps(), will take in an array of words and capitalize them, and then
    // the second function, sortWords(), will sort the words in alphabetical order.
    // If the array contains anything but strings, it should throw an error.

    const myArray = ['makeAllCaps', 'hello', 'aloha']
    const makeAllCaps = array => {
        return new Promise(((a, b) => {
            var arrayCaps = array.map(word => {
                if (typeof word === 'string') {
                    return word.toUpperCase()
                }
                else {
                    b('Error: Not all items in the array are strings')
                }
            });
            a(arrayCaps);
        }))
    }

    const sortWords = array => {
        return new Promise(((a, b) => {
            if (array) {
                array.forEach((ele) => {
                    if (typeof ele !== 'string') {
                        b('Error!')
                    }
                });
                a(array.sort())
            }
            else {
                b('Something went wrong with the sorting');
            }
        }))
    }

    makeAllCaps(myArray)
        .then(sortWords)
            .then((result) => console.log(result))
            .catch((error) => console.log(error));

})()


