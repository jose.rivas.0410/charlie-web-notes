"use script"

// TODO: Create an object called movie. Include four properties. Console log each property
// var movie = {};
//
//     movie.name = 'A Haunted House';
//     movie.director = 'Michael Tiddes';
//     movie.genre = "Comedy";
//     movie.bingeWatch = "Yes";
//
// movie.features =
//
//     function () {
//
//         console.log("Movie Name: " + this.name
//             + " \nDirector of Movie: " + this.director
//             + " \nGenre of Movie: " + this.genre
//             + " \nBinge Watch Worthy?: " + this.bingeWatch);
//     };
//
// movie.features();

// TODO: Create an object called movie include four properties of title, protagonist, antagonist, genre.
//  Console log each property
// var movie = {};
//
// movie.title = 'Avengers: Infinity War';
// movie.protagonist = 'Thanos';
// movie.antagonist = "Marvel Heroes";
// movie.genre = ["Action", " Adventure", " Sci-Fi"];
//
// movie.features =
//
//     function () {
//
//         console.log("Movie Name: " + this.title
//             + " \nProtagonist of Movie: " + this.protagonist
//             + " \nAntagonist of Movie: " + this.antagonist
//             + " \nGenre of Movie: " + this.genre);
//     };
//
// movie.features();

// TODO: Create a pet/companion object include the following properties:
//  string name, number age, string species, number valueInDollars,
//  array nickNames, vaccinations: date, type, description.
// var pet = {};
//
//     pet.name = 'Kronk'
//     pet.age = 4
//     pet.species = 'Chimú'
//     pet.valueInDollars = 100
//     pet.nickNames = ["El Emperor", " Kronkie", " The Big K"]
//     pet.vaccine = ["Date: 04/11/20", " Type: Rabies", " Description: Was fighting the doctor one on one"]
//
// pet.description =
//
//     function () {
//         console.log("Pets Name: " + this.name
//         + " \nAge of Pet: " + this.age
//         + "\nCompanions Species: " + this.species
//         + "\nPets Value in Dollars: $" + this.valueInDollars
//         + "\nNicknames of Companion: " + this.nickNames
//         + "\nVaccinations of Pet: " + this.vaccine);
//     };
// pet.description();

// TODO: Create an array of 4 objects called movies that include four properties,
//  One property being an array of objects.
//  One function named myReview() for each movie
//  Loop through the array to display and console log the 'Released month, release day, and release year'.
//  Loop through the array to display and console log the 'Title, Tooten Score: , My review: ., and Awards: '
//  Loop through the array to display and console log the 'Director: FirstName LastName'
const movies = [
    {
        title: 'To Kill A Mockingbird',
        date: {
            day: '16',
            month: 'March',
            year: '1962',
        },
        genre: ['Crime', ' Drama'],
        directors: {
            firstName: 'Robert',
            lastName: 'Mulligan',
        },
        myReview: function () {
            // console.log();
        },
        awards: ['Best Actor in a Leading Role', ' Best Art Direction-Set Decoration, Black-and-White',
            ' Best Actor - Drama', ' Best Foreign Actor (Migliore Attore Straniero)', ' Top General Entertainment',
            ' Best Written American Drama']
    },
    {
        title: "Singin' in the Rain",
        date: {
            day: '11',
            month: 'April',
            year: '1962',
        },
        genre: ['Comedy', ' Musical', ' Romance'],
        directors: {
            firstName: 'Gene',
            lastName: 'Kelly',
        },
        myReview: function () {
            // console.log();
        },
        awards: ['Best Actor - Comedy or Musical', ' Top Ten Films', ' Motion Picture',
            ' Best Written American Musical']
    },
    {
        title: 'Avengers: Endgame',
        date: {
            day: '26',
            month: 'April',
            year: '1962',
        },
        genre: ['Action', ' Adventure', ' Drama'],
        directors: {
            firstName1: 'Anthony',
            lastName1: 'Russo',
            firstName2: 'Joe',
            lastName2: 'Russo',
        },
        myReview: function () {
            // console.log();
        },
        awards: ['Outstanding Action Performance by a Stunt Ensemble in a Motion Picture',
            ' Feature Film VFX - Gold', ' Fantasy Film', ' Best Motion Capture/Special Effects Performance']
    },
    {
        title: 'Godzilla: King of the Monsters',
        date: {
            day: '31',
            month: 'May',
            year: '1962',
        },
        genre: ['Action', ' Adventure', ' Fantasy'],
        directors: {
            firstName: 'Michael',
            lastName: 'Dougherty',
        },
        myReview: function () {
            // console.log();
        },
        awards: ['Feature Film VFX', ' Best Special Effects', ' Outstanding Sound - Feature Film',
            ' Outstanding Sound - Theatrical Feature', ' Best Vocal/Motion Capture Performance',
            ' ORIGINAL SCORE FOR A FANTASY/SCIENCE FICTION/HORROR FEATURE',]

    }
];

    movies.forEach(function (movie) {

            console.log(movie.date)

    });

    movies.forEach(function (movie) {

        console.log(movie.directors)

    });

// TODO: ADDITIONAL OBJECTS PRACTICE:
//  1. Create an object named 'dreamCar' with as many properties that
//  describes your 'Dream Car/Truck/etc.'
//  2. Create an array of objects named 'videoGames'. Each object should have a
//  title, year of released, develop company, and creator: {firstName:, lastName}
//  - extra challenge: create a property named 'creatorFullName' that has a function
// var dreamCar = {};
//
//     dreamCar.make = 'Alfa Romeo';
//     dreamCar.model = 'Giulia Ti';
//     dreamCar.engineType = "I4-Automatic";
//     dreamCar.numberOfTires = 4;
//     dreamCar.safety = ["Power Steering", " Sensors", " Anti-Theft System", " Cruise Control", " Navigation", " DNA Driving Mode"];
//     dreamCar.typeOfCar = 'Sedan';
//
//     dreamCar.features =
//
//      function () {
//
//          console.log("Make of car: " + this.make
//              + " \nModel of car: " + this.model
//              + " \nType of engine: " + this.engineType
//              + " \nNumber of tires: " + this.numberOfTires
//              + " \nSafety features of the car: " + this.safety
//              + " \nWhat type of car it is: " + this.typeOfCar);
//      };
//
//     dreamCar.features();
