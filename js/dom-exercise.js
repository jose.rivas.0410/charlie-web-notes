"use strict"

// TODO: When the button with the id of `change-bg-color` is clicked the background of
//  the page should turn blue.*/
const btnBgChangeColor = document.getElementById('change-bg-color');

function changeBgColor() {

        document.body.style.background = 'blue';

}

btnBgChangeColor.addEventListener('click', function () {
    changeBgColor();
});

// TODO: When the button with an id of `append-to-ul` is clicked, append an li with
//  the content of `text` to the ul with the id of `append-to-me`
const addBtn = document.getElementById('append-to-ul');

    function addLiToList() {
        const ul = document.getElementById('append-to-me');


        const li = document.createElement("li");

        li.appendChild(document.createTextNode('text'));

        ul.appendChild(li);

    }

addBtn.addEventListener('click', function () {
    addLiToList()
})

// TODO: Two seconds after the page loads, the heading with the id of `message` should
//  change it's text to "Goodbye, World!"
const headerText = document.getElementById('message');
setTimeout(function() {
    headerText.innerHTML = 'Goodbye, World!';
}, 2000);

// TODO: When a list item inside of the ul with the id of `hl-toggle` is first
//  clicked, the background of the li that was clicked should change to
//  yellow. When a list item that has a yellow background is clicked, the
//  background should change back to the original background.
const changeBgLiColor = document.getElementById('hl-toggle');
    function yellowBg() {
        changeBgLiColor.style.backgroundColor = "yellow";
    }

    changeBgLiColor.addEventListener('dblclick', function () {
        yellowBg();
    });

    function returnBg() {
        changeBgLiColor.style.backgroundColor = "blue";
    }

    changeBgLiColor.addEventListener('click', function () {
        returnBg();
    });

// TODO: When the button with the id of `upcase-name` is clicked, the element with the
//  id of `output` should display the text "Your name uppercased is: " + the
//  value of the `input` element with the id of `input` transformed to uppercase.
const outPut = document.getElementById('output');

const inPut = document.getElementById('input');

const displayUpcase = document.getElementById('upcase-name');

    function upcaseName() {
        const userInputUpcase = inPut.value.toUpperCase();

        outPut.innerHTML = 'Your name is: ' + userInputUpcase
    }

displayUpcase.addEventListener('click', function () {
    upcaseName();
});

// TODO: Whenever a list item inside of the ul with the id of `font-grow` is _double_
//  clicked, the font size of the list item that was clicked should double
const doubleClick = document.getElementById("font-grow")
    function clickDble() {

        doubleClick.style.color = "red";
        doubleClick.style.fontSize = "30px";

    }
doubleClick.addEventListener('dblclick', function () {
    clickDble();
});