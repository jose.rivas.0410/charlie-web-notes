'use strict';

const companies = [
    {
        name: 'Walmart',
        category: 'Retail',
        start_year: 1962,
        location: {
            city: 'Rogers',
            state: 'AR'
        }
    },
    {name: 'Microsoft', category: 'Technology', start_year: 1975, location: { city: 'Albuquerque', state: 'NM' }},
    {name: 'Target', category: 'Retail', start_year: 1902, location: { city: 'Minneapolis', state: 'MN' }},
    {name: 'Wells Fargo', category: 'Financial', start_year: 1852, location: { city: 'New York', state: 'NY' }},
    {name: 'Amazon', category: 'Retail', start_year: 1994, location: { city: 'Bellevue', state: 'WA' }},
    {name: 'Capital One', category: 'Financial', start_year: 1994, location: { city: 'Richmond', state: 'VA' }},
    {name: 'CitiBank', category: 'Financial', start_year: 1812, location: { city: 'New York', state: 'NY' }},
    {name: 'Apple', category: 'Technology', start_year: 1976, location: { city: 'Cupertino', state: 'CA' }},
    {name: 'Best Buy', category: 'Retail', start_year: 1966, location: { city: 'Richfield', state: 'MN' }},
    {name: 'Facebook', category: 'Technology', start_year: 2004, location: { city: 'Cambridge', state: 'MA' }},
];
// map all of the company names
const companyNames = companies.map(n => n.name);
// console.log(companyNames);

// filter all the company names started before 1990
const companyNamesYear = companies.filter(nim => nim.name && nim.start_year < 1990);
// console.log(companyNamesYear);

// filter all the retail companies
const companyRetail = companies.filter(a => a.category === 'Retail');
// console.log(companyRetail);

// find all the technology and financial companies
const companyRetail2 = companies.filter(v => v.category === 'Technology' || v.category === 'Financial');
// console.log(companyRetail2);

// filter all the companies that are located in the state of NY
const companyLocation = companies.filter(loc => loc.location.state === 'NY');
// console.log(companyLocation);

// find all the companies that started on an even year.
const evenYear = companies.filter(compYr => compYr.start_year % 2 === 0);
// console.log(evenYear);

// use map and multiply each start year by 2
const multiplyYear = companies.map(num => num.start_year * 2);
// console.log(multiplyYear);

// find the total of all the start year combined.
const totalStartYear = companies.reduce((total, store) => {
    return total + store.start_year;
}, 0);
// console.log(totalStartYear);

// display all the company names in alphabetical order
const companyNamesAlp = companies.map(alp => alp.name);
companyNamesAlp.sort();
// console.log(companyNamesAlp);

// display all the company names for youngest to oldest.
const filter8 = companies.map(x => x.start_year)
filter8.sort()
const reverseFilter = filter8.reverse()
console.log(reverseFilter)
