"use strict"

// TODO: Add jQuery code that will change the background color of a 'h1' element when clicked.
// $('#hello-from').click(function () {
//     $(this).css('background-color', 'blue');
// });

// TODO: Make all paragraphs have a font size of 18px when they are double clicked.
// $('p').dblclick(function () {
//     $(this).css('font-size', '18px');
// });

// TODO: Set all 'li' text color to green when the mouse is hovering, reset to black when it is not.
// $('li').hover(
//     function () {
//         $(this).css('color', 'green');
//     },
//     function () {
//         $(this).css('color', 'black');
//     }
// );

// TODO: Add an alert that reads "Keydown event triggered!" everytime a user pushes a key down
// $('#for-tests').keydown(function () {
//     alert("Keydown event triggered!");
// });

// TODO: Console log "Keyup event triggered!" whenever a user releases a key.
// $('#for-tests').keyup(function () {
//     console.log("Keyup event triggered!");
// });

// TODO: BONUS: Create a form with one input with a type set to text. Using jQuery, create a counter for everytime a key is pressed.
$('#for-tests').keyup(function () {
    console.log(this.value.length);
    $('#counter').text(this.value.length)
})
