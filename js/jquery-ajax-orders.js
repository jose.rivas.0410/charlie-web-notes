"use strict";

// BASIC REQUEST
// $.ajax("/some-url")

// defaults to a GET request

// AJAX OPTIONS
// $.ajax("/some-url",{
//     type: "POST",
//     data: {
//         name: "John",
//         location: "San Antonio"
//     }
// });

// The easiest way to manipulate the ajax options to pass a JavaScript object
/*
Most common options
type - type of http request to send to the server:
"GET", "POST", or "DELETE"

data - data to be included with request.

datatype - the type of data we expect the server to send back from our request
    - common options: "json", "xml", "html", or "text"

url - rather than passing the request URL as a string, you can pass a
JS object on its own

username & password - if a server requires username and password you can
specify these parameters

headers - an object of whose key value pairs represents custom HTTP headers
to send along with the request
 */

// HANDLING RESPONSES

// $.ajax("/some/path/to-a/file.json").done(function (data, status, jqXhr) {
    /// what we want the code to do or want to do with the data being pulled...

    // data - the body of the response from the server

    //status - a string indicating the status of the request

    // jqXhr - a jQuery object that represents the AJAX request


// });

// More methods for handling responses
/*
.fail()
.always()
 */

var ajaxRequest = $.ajax("data/orders.json");

ajaxRequest.done(function (data) {

    console.log(data)

    var dataHTML = buildHTML(data);
    $("#orders").html(dataHTML)

});



function buildHTML(orders) {

    var orderHTML = '';
    orders.forEach(function (order) {

        orderHTML += "<section>";
        orderHTML += "<dl>";
        orderHTML += "<dt>" + "Ordered Item:" + "</dt>";
        orderHTML += "<dd>" + order.item + "</dd>";
        orderHTML += "<dt>" + "Ordered By:" +"</dt>";
        orderHTML += "<dd>" + order.orderedBy + "</dd>"
        orderHTML += "</dl>";
        orderHTML += "</section>"

    });
    return orderHTML
}


