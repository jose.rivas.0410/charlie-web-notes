"use script"

// TODO: While Loops: Create a while loop that uses console.log() to create the output shown below
var num = 2;
    while (num <= 65536) {
        console.log(`#` +num);
        num *= 2;
    }

// TODO: For Loops: Create a function name showMultiplicationTable that accepts a number
//  and console.logs the multiplication table for that number
function showMultiplicationTable(num) {
        for (var i = 1; i <= 10; i ++) {
            // local variable
            var answer = num * i;
            console.log(`${num} x ${i} = ${answer}`);
        }
}
showMultiplicationTable(10)

// TODO: Create a for loop that uses console.log to create the output shown below.
for (var n = 100; n >= 5; n -= 5) {
    console.log(`#` + n);
}

// TODO: Prompt the user for an odd number between 1 and 50. Use a loop and a break
//  statement to continue prompting the user if they enter invalid input. Use a loop and the
//  continue statement to output all the odd numbers between 1 and 50, except for the number the user entered.
var userInput = prompt("Enter a number between 1-50")
    if (userInput > 0 && userInput < 51) {
        console.log("Number to skip: " + userInput)

        for (var o = 1; o <= 50; o ++) {
            // if statement prints out odd numbers
            if (o % 2 === 0) {
                continue;
            }
            // if statement checks if o statement matches the userInput number
            if (o === parseInt(userInput)) {
                console.log("Yikes! Skipping Number: " + o);
            }
            else {
                console.log(`Here is an odd #: ` + o);
            }
        }
    }
    else {
        alert("Invalid Input: Enter a number 1-50")
    }

// TODO: Write a for loop that will iterate from 0 to 20. For each iteration, it will check
//  if the current number is even or odd, and report that to the screen
for (var t = 0; t <= 20; t ++) {
    if (t % 2 === 0) {
        console.log(t + ` is even`);
    }
    if (t % 2 !== 0) {
        console.log(t + ` is odd`);
    }
}

// TODO: Write a for loop that will iterate from 0 to 10. For each iteration of the for loop,
//  it will multiply the number by 9 and log the result
for (var iteration = 0; iteration <= 10; iteration ++) {
    console.log(`${iteration} * 9 = ${iteration * 9}`);
}

// TODO: Break and ContinuePrompt the user for an odd number between 1 and 50
// for (var x = 1; x <= 50; x ++) {
//     if (x % 2 === 0) {
//         continue;
//     }
//     console.log(x);
// }
var continuePrompt = prompt("Enter a number between 1-50!")
    if (continuePrompt > 0 && continuePrompt < 50) {
        console.log("Skip Number: " + continuePrompt)

        for (var b = 1; b <= 50; b ++) {
            // if statement prints out odd numbers
            if (b % 2 === 0) {
                continue;
            }
            // if statement checks if o statement matches the userInput number
            if (b === parseInt(userInput)) {
                console.log("Yikes! Skipping Number: " + b);
            }
            else {
                console.log(`Here is an odd #: ` + b);
            }
        }
    }
    else {
        alert("Invalid Input: Enter a number 1-50")
    }

// TODO: BONUS: Write a program that finds the summation of every number from 1 to num.
//  The number will always be a positive integer greater than 0.
var summation = function (num) {
    // local variable
    var total = 0

    // for loop
    for (var i = 1; i <= num; i++) {
        total += i;
    }
    return total;
};
    console.log(summation(8));