// TODO: Complete the following in a new js file named 'more-es6-drills.js'
const petName = 'Chula';
const age = 14;


// TODO: REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + petName +
//     ' and she is ' + age + ' years old.');
console.log(`My dog is named ${petName} and she is ${age} years old.`)


// TODO: REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//     return num1 + 10;
// }
const num1 = 10;
const total = num1 => num1 + 10;
console.log(total(21))


// TODO: REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//     return num1 - 5;
// }
const minusFive = num3 => num3 - 5;
console.log(minusFive(10))


// TODO: REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//     return num1 * 2;
// }
const multiplyByTwo = num4 => num4 * 2;
console.log(multiplyByTwo(3))


// TODO: REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//     return "Hello, " + name + ' how are you?';
// };
const greetings = name => `Hello, ${name} how are you?`;
console.log(greetings('Jose'))


// TODO: REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//     if (name === 'Bob') {
//         return name + "Nice to see you again";
//     } else {
//         return "Nice to meet you"
//     }
// }
const haveWeMet = name => (name === 'Bob') ? "Nice to meet you again": "Nice to meet you";
// '?' replaces if
// ':' replaces else
console.log(haveWeMet("Nancy"))