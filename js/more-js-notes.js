"use strict"
// defines that js code should be executed in
// "strict mode"
// with strict mode, you cannot use undeclared variables

// CONDITIONAL STATEMENTS

// if statements
// - allow you to execute code based on certain conditions

// syntax
// if (condition) {
//    code here will be executed if condition is true
// }

// example

// var numberOfLives = 1;
// if (numberOfLives ===0) {
//     alert("Game Over!");
// }

// if / else statements
// else statement - execute when the condition
// in the if statement is false

// syntax
// if (condition) {
//    code here will be executed if condition is true
// }
// else {
//     code here will be executed if condition is false
// }

// example
var b = 1;

// if (b === 10) {
//     console.log("b is 10");
// }
// else {
//     console.log("b is NOT 10");
// }

// else if statement
if (b === 10) {
    console.log("b is 10");
}
else if (b < 10) {
    console.log("b is less than 10")
}
else {
    console.log("b is NOT entered");
}

// SWITCH STATEMENTS
// less duplications and it increase readability in code

// syntax
/*
switch (condition) {
    case // code that gets executed
    break // stops the code when case is executed
    case ''
    break;
    ..
    ..
    default '' // equivalent to the else statement
    break;
}
 */

// example
var color = "red";
switch (color) {
    case "blue":
        console.log("You chose blue")
        break;
    case "red":
        console.log("You chose red")
        break;
    case "pink":
        console.log("You chose pink")
        break;
    case "green":
        console.log("You chose green")
    default:
        console.log("You didn't select the right color")
        break;
}

// using a switch statement
// create a program for the weather
// sunny, cloudy, rain, snow, default
var weather = "sunny"
switch (weather) {
    case "rain":
        console.log("It is pouring rain like it's Jupiter up in here")
        break;
    case "snow":
        console.log("It's snowing like you're in the antarctic")
        break;
    case "sunny":
        console.log("It's sunny out like it's the sahara")
        break;
    case "cloudy":
        console.log("It's just cloudy")
        break;
    default:
        console.log("The weather is not known on this planet")
        break;
}


// TERNARY OPERATOR
// shorthand way of creating if/else statement
// only used when there are two choices

// syntax
// (if condition is true) ? run this code : otherwise run this code

// example
var numberOfLives = 6;

(numberOfLives === 0) ? console.log("Game Over!") : console.log("Still ALive!")


// if / else if / else

var burgerPreference = prompt("What kind of burger do you like?");
if (burgerPreference === "Bacon and Cheese") {
    alert("Bacon and Cheese burgers are the bomb!");
}
else if (burgerPreference === "Chili") {
    alert("What a Joe!");
}
else if (burgerPreference === "Cheese") {
    alert("Plain but good!");
}
else if (burgerPreference === "Double Cheese") {
    alert("Gonna get the runs boy!");
}
else {
    alert("Good Burger");
}