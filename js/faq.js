"use strict";

// For showing all answer's in dl
// $('dd').addClass('invisible');
// $('.show-toggle').click(function(event){
//     event.preventDefault();
//     $('dd').toggleClass('invisible');
// });

// for each dl
$('#iv1').addClass('invisible');
$('#show-toggle1').click(function(event){
    event.preventDefault();
    $('#iv1').toggleClass('invisible');
});

$('#iv2').addClass('invisible');
$('#show-toggle2').click(function(event){
    event.preventDefault();
    $('#iv2').toggleClass('invisible');
});

$('#iv3').addClass('invisible');
$('#show-toggle3').click(function(event){
    event.preventDefault();
    $('#iv3').toggleClass('invisible');
});

$('#iv4').addClass('invisible');
$('#show-toggle4').click(function(event){
    event.preventDefault();
    $('#iv4').toggleClass('invisible');
});

$('#iv5').addClass('invisible');
$('#show-toggle5').click(function(event){
    event.preventDefault();
    $('#iv5').toggleClass('invisible');
});

$('#iv6').addClass('invisible');
$('#show-toggle6').click(function(event){
    event.preventDefault();
    $('#iv6').toggleClass('invisible');
});

$('#iv7').addClass('invisible');
$('#show-toggle7').click(function(event){
    event.preventDefault();
    $('#iv7').toggleClass('invisible');
});

$('#iv8').addClass('invisible');
$('#show-toggle8').click(function(event){
    event.preventDefault();
    $('#iv8').toggleClass('invisible');
});

$('#iv9').addClass('invisible');
$('#show-toggle9').click(function(event){
    event.preventDefault();
    $('#iv9').toggleClass('invisible');
});

$('#iv0').addClass('invisible');
$('#show-toggle0').click(function(event){
    event.preventDefault();
    $('#iv0').toggleClass('invisible');
});

$('#question').hide(
    function () {
        $('#question').fadeIn(10000);
    }
);

$('#button-color').click(function () {
    $('dd').css('background-color', 'orange');
});

$('#x-marker').click(function () {
    $('#question2').hide();
});

$('#alert').fadeOut(8000)