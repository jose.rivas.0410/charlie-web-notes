"use script"

// FOR LOOP
// basic syntax
/*
for (initialization; condition; increment/decrement) {
    // run the code
}
 */

// for (var i = 100; i >= 50; i--) {
//     console.log(i);
// }

// forEach (used only for arrays)
// basic syntax
/*
nameOfVariable.forEach(function(element, index, array) {
    ... run code here

})
 */

// syntax of function
/*
function nameOfFunction() {
}
 */
var i = [100,98,78,68];
i.forEach(function (i1) {
    console.log(i1);
})

