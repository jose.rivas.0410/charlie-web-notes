"use strict";
// MOUSE EVENT(S)
/*
.click() - event handler to the 'click'
.dblclick() - event handler to the 'double click'
.hover() - executed when the mouse pointer enters and leaves
the element(s)

 */

// MOUSE EVENT IN JS
/*
var eventElement = document.getElementById('#my-id')

eventElement.addEventListener('click', function () {
    // code to get executed
}
 */

// MOUSE EVENT IN jQuery
/*
syntax :
$('selector').click( handler/function() {
    // run code
});
 */

// $('#charlie').click(function () {
//     alert("The h1 element with the id of charlie was clicked!")
// });

// $('#charlie').dblclick(function () {
//     alert("h1 element with the id of charlie was double clicked!")
// });

// research '.dbleclick()' vs '.click()'

// .hover()
//$('selector').hover( handlerIn, handlerOut)

// $('#charlie').hover(
//     //handlerIn anonymous function
//     function () {
//         $(this).css('background-color', 'skyblue');
//     },
//     // handlerOut anonymous function
//     function () {
//         $(this).css({'background-color': 'white', 'font-size': '24px'});
//     }
// );


// KEYBOARD EVENTS IN jQuery
/*
Different types of keyboard events
    .keyboard()
    .keypress()
    .keyup()
    .on()
    .off()

 */

// .keydown() - event handler to the 'keydown'
// JS event / triggers that event on an element

// syntax: $('selector').keydown( handler )
// $('#my-form').keydown(function () {
//     alert("Hey! You pushed down a key in the form!")
// });

// .keypress()
/*
same as keydown, with one exception:
Modifier keys (shift, control, esc...)
- will trigger .keydown() but NOT keypress()
 */

// .keyup()
$('#my-form').keyup(function () {
    alert("Hey! You pushed down a key in the form!")
});


// .on() .off()

