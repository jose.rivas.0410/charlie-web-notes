// old way
/*
alert(Math.pow(2, 8)); 256
 */

// es6 way
/*
alert(2**8); // 256
 */

/*
if(true) {
    var cohort = 'charlie'
    }
if(true) {
    const cohort = 'charlie'
    {
 */

/*
var x = 10;
    // here x is 10
{
    const x = 2,
    // here x is 2
}
console.log(x)
// here x is 10
 */

// FOR ... OF
// SYNTAX
/*
for(const element of iterable) {

}
 */

// Example
// const x = [10, 20, 30];
//
// for (const value of x)(
//     console.log(value)
// )