"use strict"

// Number prompt
// confirm("Would you like to enter a number?");

// if (confirm("Would you like to enter a number") === true) {
//     prompt("Enter a number");
// }

function runfunction() {
    var input = prompt("Insert a number");
    var num = parseInt(input);

    if (isNaN(num)) {
        alert("Not a number!");
    }

    else {
        alert(`${num} + 100 is ${num + 100}`);

        // num is even or odd
        if (num % 2 === 0) {
            alert(`${num} is an even`);
        } else {
            alert(`${num} is odd`);
        }

        if (num ===0) {
            alert("0 is neither positive or negative")
        }
        else if (num > 0) {
            alert(`${num} is positive`);
        }
        else {
            alert(`${num} is negative`);
        }
    }
}
if (confirm("Would you like to enter a number") === true) {
    runfunction()
} else {
    alert(`Okay, Bye`);
}

// Color input
function color(input) {
    if (input === "blue") {
        alert("Blue is the color of the ocean");
    }
    else if (input === "red") {
        alert("Red is known as power for dragons");
    }
    else {
        alert(`${input} is a unique color`);
    }
}
color("yellow");

// Discount raffle
function calculateTotal(luckyNumber, totalAmount) {
    if (luckyNumber === 0) {
        alert(`Your total amount with discount is $${totalAmount}`);
    }
    else if (luckyNumber === 1) {
        alert(`Your total amount with discount is $${totalAmount - totalAmount * .10}`);
    }
    else if (luckyNumber === 2) {
        alert(`Your total amount with discount is $${totalAmount - totalAmount * .25}`);
    }
    else if (luckyNumber === 3) {
        alert(`Your total amount with discount is $${totalAmount - totalAmount * .35}`);
    }
    else if (luckyNumber ===4) {
        alert(`Your total amount with discount is $${totalAmount - totalAmount * .50}`);
    }
    else if (luckyNumber === 5) {
        alert(`Your total amount with discount is $${totalAmount - totalAmount * .75}`);
    }
    else {
        alert(luckyNumber + "is invalid");
    }
}
calculateTotal(5, 100);

// Grading system
var grades = prompt("Enter your grade percentage");
    if (grades >= 97) {
        alert("You have an A+!");
    }
    else if (grades >= 94) {
        alert("You have an A!");
    }
    else if (grades >= 90) {
        alert("You have an A-!");
    }
    else if (grades >= 87) {
        alert("You have a B+!");
    }
    else if (grades >= 84) {
        alert("You have a B!");
    }
    else if (grades >= 80) {
        alert("You have a B-!");
    }
    else if (grades >= 77) {
        alert("You have a C+!");
    }
    else if (grades >= 74) {
        alert("You have a C!");
    }
    else if (grades >= 70) {
        alert("You have a C-!");
    }
    else if (grades >= 67) {
        alert("You have a D+!");
    }
    else if (grades >= 64) {
        alert("You have a D!");
    }
    else if (grades >= 60) {
        alert("You have a D-!");
    }
    else {
        alert("You have an F!");
    }

// Military Time
function militaryTime(time) {
    if (time < 12 && time <= 0) {
        alert("Good Morning");
    }
    else if (time < 18) {
        alert("Good Afternoon");
    }
    else if (time > 18 && time <= 24) {
        alert("Good Evening");
    }
}
    militaryTime(17);

// Season system
function seasonReminder(season) {
        if (season === "December" || season === "January" || season === "February") {
            alert("Winter");
        }
        else if (season === "March" || season === "April" || season === "May") {
            alert("Spring");
        }
        else if (season === "June" || season === "July" || season === "August") {
            alert("Summer");
        }
        else if (season === "September" || season === "October" || season === "November") {
            alert("Fall");
        }
        else {
            alert("Month not recognized");
        }
}
    seasonReminder("September");