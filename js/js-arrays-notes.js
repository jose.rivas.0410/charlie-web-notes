"use script"

// Arrays
// - a data structure that holds an ordered list of items
// - a variable that contains a list of values.

/*
{} braces
[] brackets
BASIC SYNTAX
[] an empty array
['red']
['red', 'blue', 'white'] 3 elements
[4, 4.4, -14, 0] an array of numbers, 4 elements
['red', 4, true, -4]

 */

// var myArray = ['black', 12, -144, false];
// console.log(myArray);
// console.log(myArray.length); // 4
// console.log(myArray[0]); // black
// console.log(myArray[4]); // undefined

// var charlie = ['josh', 'nick', 'jose', 'karen', 'stephen'];
// console.log(charlie);
// // console.log(charlie.length); // 4
// // console.log(charlie[0]); // black
// // console.log(charlie[4]); // undefined
// console.log("There are " + charlie.length + " members in the " +
//     "Charlie cohort");

// ITERATING ARRAY
// - transverse through elements
var charlie = ['josh', 'nick', 'jose', 'karen', 'stephen'];
// console.log(charlie);

// console.log("There are " + charlie.length + " members in the " +
//     "Charlie cohort");

// LOOP THROUGH AN ARRAY AND CL THE VALUES
// for (var i = 0; i < charlie.length; i ++) {
//     // print out all the values for charlie
//     // individually
//     console.log("The person at index " + i + " is " + charlie[i]);
// }

// forEach loop
/*
Syntax:
nameOfVariable.forEach( function (element, index, array)  {
    .. run the code
});
 */

// example
var ratings = [25, 34, 57, 62, 78, 89, 91];

// ratings.forEach(function (rating) {
//     console.log(rating)
// });

var pizza = ["cheese", "dough", "sauce", "pineapple", "canadian bacon"];
// pizza.forEach(function (ingredients ) {
//     console.log(ingredients);
// });

// CHANGING / MANIPULATING ARRAYS
var fruits = ['banana', 'apple', 'grape', 'strawberry'];
// console.log(fruits);

// adding to the array
// .push - add the end of the array
fruits.push('watermelon');
// console.log(fruits);

// .unshift - add to the beginning of an array
fruits.unshift('dragon fruit');
// console.log(fruits);
// console.log(fruits[0]);

fruits.push('cherry', 'mango', 'pineapple');
// console.log(fruits);

// REMOVE FROM ARRAY
// .pop - will remove the last element of an array
fruits.pop();
// console.log(fruits);

// .shift() - will remove the first element of an array
fruits.shift();
// console.log(fruits);

var removedFruits = fruits.shift();
    // console.log("Fruits Removed: " + removedFruits);


// LOCATING ARRAY ELEMENTS
console.log(fruits)

// .indexOf - returns the first occurrence of what you're looking
// for and the index

var indexElement = fruits.indexOf('watermelon');
console.log(indexElement);

// SLICING
// .slice - copies a portion of the array
// and returns a new array (does not modify the original)

var slice = fruits.slice(2, 5);
console.log(slice);

// REVERSE
// .reverse - will reverse the original array
// AND returns the original array
fruits.reverse();
console.log(fruits);

// SORT
// .sort - will sort the original array
// and return the sorted array
console.log(fruits.sort());

// SPLIT
// .split - takes a string and turns it into an array
var codebound = "leslie,stephen,karen";
var codeboundArray = codebound.split(',');
console.log(codeboundArray);

// JOIN
// .join - takes an array and converts it to a string
// with a delimiter of your choice
var newCodebound = codeboundArray.join(',');
console.log(newCodebound);







