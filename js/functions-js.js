// alert system method
//const word = prompt("Type any word you like and I'll tell you the number of characters");
    //alert( `Your total amount of words come to ${word.length}`);

// JavaScript inspect console method
function count(word) {
    return word.length;
}

    console.log(`My character count for my sentence is ${count("This is my sentence that will show me the length.")}`);

function add(num1, num2) {
    console.log(`The sum of both numbers are ${num1 + num2}`);
}

add(31, 58);

function subtract(num3, num4) {
    console.log(`Both numbers subtracted equal ${num3 - num4}`);
}

subtract(94, 40);

function multiply(num5, num6) {
    console.log(`Both numbers multiplied by one another equal ${num5 * num6}`);
}

multiply(8, 5);

function divide(numerator, denominator) {
    console.log(`Both numbers divided equal ${numerator / denominator}`);
}

divide(144, 12);

function remainder(number, divisor) {
    console.log(`The remainder from both numbers equal ${number % divisor}`);
}

remainder(142, 7);

function square(num01) {
    return num01 ** 2;
}

    console.log(`The number squared equals ${square(32)}`);

// second functions exercise
function isOdd(x1) {
    return x1 % 2 !== 0;
}

    console.log(`The answer is ${isOdd(4)}`);

function isEven(x) {
    return x % 2 === 0;
}

    console.log(`The answer is ${isEven(6)}`);

function isSame(input1,input2) {
    return input1 === input2;
}

    console.log(`Both value and datatype are ${isSame("Hamburger", "Hamburger")}`);

function isSeven(x) {
    return x === 7;
}

    console.log(`Number value of seven is ${isSeven(7)}`);

function addTwo(num1) {
    return num1 + 2;
}

    console.log(`Value plus two equals ${addTwo(35)}`);

function isMultipleOfTen(x) {
    return x % 10 === 0;
}

    console.log(`Is 70 a multiple of 10? ${isMultipleOfTen(70)}`);

function isMultipleOfTwo(x) {
    return x % 2 === 0;
}

    console.log(`Is 40 a multiple of 2? ${isMultipleOfTwo(40)}`);

function isMultipleOfTwoAndFour(x) {
    return x % 2 === 0 && x % 4 ===0;
}

    console.log(`Is 16 a multiple of 2 and 4? ${isMultipleOfTwoAndFour(16)}`);

function isTrue(x) {
    return x === "true"
}

    console.log(`Is the value true or false? ${isTrue("true")}`);

function isFalse(x) {
    return x === "false"
}

    console.log(`Is the value true or false? ${isFalse("true")}`);

function isVowel(word) {
    return word === "a" || word === "A" || word === "e" || word === "E"||
        word === "i" || word === "I"|| word === "o" || word === "O" || word === "u"
        || word === "U";
}

    console.log(`Is 'a' a vowel? ${isVowel("a")}`);
    console.log(`Is 'E' a vowel? ${isVowel("E")}`);

function triple(num1) {
    return num1 * 3;
}

    console.log(`Number times 3 is ${triple(5)}`);

function QuadNum(num1) {
    return num1 * 4;
}

    console.log(`Number times 4 is ${QuadNum(2)}`);

function modulus(a, b) {
    return a % b;
}

    console.log(`Remainder is ${modulus(30,9)}`);

function degreesToRadians(degree) {
    var pi = Math.PI
    return degree * (pi/180);
}

    console.log(`The radian is ${degreesToRadians(45)}`);

// My accident
// function absoluteValue(x) {
//     return x = Math.abs(5 + 5);
// }
//
//     console.log(absoluteValue(absoluteValue(Math.abs(5 + 5))));
function absoluteValue(x) {
    // parseInt turns strings into integers
    return parseInt(x);
}

    console.log(absoluteValue("10"));
    console.log(absoluteValue("Stephen")); // NaN - not a number

function reverseString(x) {
    var splitString = x.split("");
    var reverseArray = splitString.reverse();
    var joinArray = reverseArray.join("");
    return joinArray;
}

    console.log(`The reverse of Hello is ${reverseString("Hello")}`);