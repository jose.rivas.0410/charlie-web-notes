// ES6 EXERCISE
// Complete the following in a new js file name es6exercise.js
/*
 * Complete the TODO items below
 */
const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'php', 'java']
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'angular', 'spring boot']
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['java', 'aws', 'php']
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['javascript', 'java', 'sql']
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql']
    }
];


// TODO: replace the `var` keyword with `const`, then try to reassign a variable as a const
// TODO: fill in your name and email and add some programming languages you know to the languages array
// const name = 'Jose'
// const email = 'jrivas@cohortcharlie.com'
// const languages = ['html','css','javascript','bootstrap','jquery']


// TODO: rewrite the object literal using object property shorthand
// developers.push({
//     name,
//     email,
//     languages,
// });

// TODO: replace `var` with `let` in the following variable declarations
let emails = [];
let names = [];
let languages = [];


// TODO: rewrite the following using arrow functions
developers.forEach(developer => emails.push(developer.email));
// console.log(emails);

developers.forEach(developer => names.push(developer.name));
// console.log(names);

developers.forEach(developer => languages.push(developer.languages));
console.log(languages)


// TODO: replace `var` with `const` in the following declaration
const developerTeam = [];
developers.forEach(function(developer) {


    // TODO: rewrite the code below to use object destructuring assignment
    //       note that you can also use destructuring assignment in the function
    //       parameter definition
    // const name = developer.name;
    // const email = developer.email;
    // const languages = developer.languages;
    const {name, email, languages} = developer


    // TODO: rewrite the assignment below to use template strings
    developerTeam.push(`${name}'s email is ${email}. ${name} knows ${languages.join(', ')}`)

    //     developerTeam.push(name + '\'s email is ' + email + name + ' knows ' + languages.join(', '));
    });
    console.log(developerTeam);


// TODO: Use `const` for the following variable
const list = '<ul>';


// // TODO: rewrite the following loop to use a for..of loop
// developers.forEach(function (developer) {
//
//
//     // TODO: rewrite the assignment below to use template strings

//         list += '<li>' + developer + '</li>';
//     });

for (const ul of developers) {
    list += `<li>${ul.name}</li>`
};

list += '</ul>';

    document.write(list);