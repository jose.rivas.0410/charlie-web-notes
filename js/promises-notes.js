"use strict";
// Promises
// a tool for handling asynchronous events

/*
A promise represents an event that might not yet
have happened.
A promise will be in one of three states:
- pending: the event has not yet to happened
- resolved: the event has happened successfully
- rejected: the event has happened and an error
condition occurred.
 */

// example
// const myPromise = new Promise((resolve, reject) => {
//     if (Math.random() > 0.5) {
//         resolve()
//     }
//     else {
//         reject()
//     }
// });
//
// myPromise.then(() => console.log('Promise Resolved'));
// myPromise.catch(() => console.log("Promise Rejected"));
// A promise object has two used methods
// .then() = accepts a callback that will run when the
// promise is resolved

// .catch() = accepts a callback that run when the promise
// is resolved

//
// const myPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if (Math.random() > 0.5) {
//             resolve()
//         }
//         else {
//             reject()
//         }
//     }, 5000);
// });
// console.log(myPromise); // a pending promise
//
// myPromise.then(() => console.log('Promise Resolved'));
// myPromise.catch(() => console.log("Promise Rejected"));

// write a promise inside a function
// function makeARequest() {
//
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (Math.random() > 0.5) {
//                 resolve("Here is your data: ...")
//             }
//             else {
//                 reject("Network Connection Error")
//             }
//         }, 5000);
//     });
//
// }
// const request = makeARequest();
// console.log(request); /// pending promise
//
//
// request.then(data => console.log('Promise Resolved', data));
// request.catch(error => console.log("Promise Rejected", error));


// use a fetch and the promises returned from it

// const myPromise = fetch('https://api.github.com/users');
// myPromise.then(response => console.log(response));
// myPromise.catch(error => console.log(error));


// chaining
// - the return value from a promise's callback that can itself
// be treated as a promise, which allows us to CHAIN promises together.

// const myPromise = new Promise()

// Promise.resolve('one').then((one) => {
//     console.log(one);
//     return 'two';
// }).then((two) => {
//     console.log(two);
//     return 'three';
// }).then((three) => {
//     console.log(three)
// });

const myPromise =
    fetch('https://api.github.com/users')
        .then((response) => {
            const users =
                response.json().then((users) => {
                    users.forEach((user) => {
                        // do something with each
                        // user object
                        console.log(user);
                    })
                })
        });
console.log(myPromise)
