"use strict";

const developers = [
    {
        name: 'stephen',
        email: 'stephen@appddictionstudio.com',
        languages: ['html', 'css', 'javascript', 'angular', 'php'],
        yearExperience: 4
    },
    {
        name: 'karen',
        email: 'karen@appddictionstudio.com',
        languages: ['java', 'javascript', 'spring boot'],
        yearExperience: 3
    },
    {
        name: 'juan',
        email: 'juan@appddictionstudio.com',
        languages: ['html', 'css', 'java', 'aws', 'php'],
        yearExperience: 2
    },
    {
        name: 'leslie',
        email: 'leslie@codebound.com',
        languages: ['node', 'npm', 'sql', 'javascript', 'java' ],
        yearExperience: 5
    },
    {
        name: 'dwight',
        email: 'dwight@codebound.com',
        languages: ['html', 'angular', 'javascript', 'sql'],
        yearExperience: 8
    }
];
/**Use .filter to create an array of developer objects where they have
 at least 5 languages in the languages array*/
const language = developers.filter(function (developer) {
    if (developer.languages.length >= 5) {
        return developer
    }
});
// console.log(language);

/**Use .map to create an array of strings where each element is a developer's
 email address*/
const emails = developers.map(num => num.email);
// console.log(emails);

/**Use reduce to get the total years of experience from the list of developers.
 * Once you get the total of years you can use the result to calculate the average.*/
const years = developers.reduce((total, person) => {
    return total + person.yearExperience;
}, 0);
// console.log(years); // 22

// average
const average = years / developers.length
// console.log(average)

/**Use reduce to get the longest email from the list.*/
const longestEmail = developers.reduce((one, two) => {
    return one.email.length > two.email.length ? one : two;
}).email;
// console.log(longestEmail);

/**Use reduce to get the list of developer's names in a single string
 - output:
 CodeBound Staff: stephen, karen, juan, leslie, dwight*/
const developerNames = developers.reduce((accumulation, person) => {
    return accumulation + ',' + person.name;
}, 'Codebound Staff: chris') + '.';
console.log(`${developerNames}`)

/** BONUS: use reduce to get the unique list of languages from the list
 of developers
 */